services:
  traefik:
    hostname: traefik
    image: traefik:v2.11.9
    user: "${PUID}:${PGID}"
    container_name: traefik
    restart: unless-stopped
    domainname: ${DOMAIN_NAME}
    networks:
      - traefik_proxy
    command:
      # Globals
      - --log.level=${TRAEFIK_LOG_LEVEL:-ERROR}
      - --global.sendAnonymousUsage=false
      - --api
      - --api.insecure=true
      - --accesslog=true
      - --accessLog.filePath=/traefik.log
      - --accessLog.bufferingSize=100 # Configuring a buffer of 100 lines
      - --accessLog.filters.statusCodes=400-499
      # Entrypoints
      - --entryPoints.http.address=:80
      - --entryPoints.https.address=:443
      # Docker
      - --providers.docker=true
      - --providers.docker.endpoint=unix:///var/run/docker.sock
      - --providers.docker.defaultrule=Host(`{{ index .Labels "com.docker.compose.service" }}.$DOMAIN_NAME`)
      - --providers.docker.exposedByDefault=false
      - --providers.docker.network=traefik_proxy
      # File provider
      - --providers.file.directory=/etc/traefik/rules
      - --providers.file.watch=true
      # LetsEncrypt
      # - --certificatesResolvers.mydnschallenge.acme.caServer=https://acme-staging-v02.api.letsencrypt.org/directory
      - --certificatesresolvers.mydnschallenge.acme.email=${CLOUDFLARE_EMAIL}
      - --certificatesresolvers.mydnschallenge.acme.dnschallenge.provider=cloudflare
      - --certificatesresolvers.mydnschallenge.acme.storage=/etc/traefik/acme.json
      - --certificatesresolvers.mydnschallenge.acme.dnschallenge.resolvers=1.1.1.1:53,1.0.0.1:53
      - --certificatesresolvers.myhttpchallenge.acme.httpchallenge=true
      - --certificatesresolvers.myhttpchallenge.acme.httpchallenge.entrypoint=http
      - --certificatesresolvers.myhttpchallenge.acme.email=${CLOUDFLARE_EMAIL}
      - --certificatesresolvers.myhttpchallenge.acme.storage=/etc/traefik/acme.json
      - --serversTransport.insecureSkipVerify=true
    ports:
      - 80:80
      - 443:443
    environment:
      - CLOUDFLARE_EMAIL=${CLOUDFLARE_EMAIL}
      - CLOUDFLARE_API_KEY=${CLOUDFLARE_API_KEY}
      - CLOUDFLARE_POLLING_INTERVAL=6
      - CLOUDFLARE_PROPAGATION_TIMEOUT=100
      - CLOUDFLARE_HTTP_TIMEOUT=60
    volumes:
      - ${DOCKER_DIR}/traefik/acme.json:/etc/traefik/acme.json
      - ${DOCKER_DIR}/traefik/rules:/etc/traefik/rules
      - ${DOCKER_DIR}/traefik/traefik.log:/traefik.log
      - ${DOCKER_DIR}/traefik/certs:/etc/traefik/certs
      - /var/run/docker.sock:/var/run/docker.sock:ro
    labels:
      # Global http-->https
      - traefik.http.routers.http-catchall.entrypoints=http
      - traefik.http.routers.http-catchall.rule=HostRegexp(`{host:.+}`) && !PathPrefix(`/.well-known/acme-challenge/`)
      - traefik.http.routers.http-catchall.middlewares=redirect-to-https
      - traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https
      # API
      - traefik.enable=true
      - traefik.http.routers.traefikui.entrypoints=https
      - traefik.http.routers.traefikui.middlewares=admin-oauth@file
      - traefik.http.routers.traefikui.rule=Host(`traefik.${DOMAIN_NAME}`) && (PathPrefix(`/api`) || PathPrefix(`/dashboard`))
      - traefik.http.routers.traefikui.tls=true
      - traefik.http.routers.traefikui.service=api@internal
      - traefik.docker.network=traefik_proxy
      # Wildcard cert
      - traefik.http.routers.traefik.tls=true
      - traefik.http.routers.traefik.tls.certresolver=mydnschallenge
      - traefik.http.routers.traefik.tls.domains[0].main=${DOMAIN_NAME}
      - traefik.http.routers.traefik.tls.domains[0].sans=*.${DOMAIN_NAME}
      - traefik.tls.stores.default.defaultgeneratedcert.resolver=mydnschallenge
      - traefik.tls.stores.default.defaultgeneratedcert.domain.main=${DOMAIN_NAME}
      - traefik.tls.stores.default.defaultgeneratedcert.domain.sans=*.${DOMAIN_NAME}
      # Global error page
      # - traefik.http.middlewares.test-errorpage.errors.status=400-599
      # - traefik.http.middlewares.test-errorpage.errors.service=nginx-proxy-manager-service@file
      # - traefik.http.middlewares.test-errorpage.errors.query=/
  ingress-editor:
    hostname: ingress-editor
    image: pldubouilh/gossa
    user: "${PUID}:${PGID}"
    container_name: ingress-editor
    restart: unless-stopped
    domainname: ingress-editor.${DOMAIN_NAME}
    networks:
      - traefik_proxy
    environment:
      - PUID
      - PGID
      - TZ
    labels:
      - traefik.enable=true
      - traefik.http.routers.ingress-editor.entrypoints=https
      - traefik.http.routers.ingress-editor.tls=true
      - traefik.http.routers.ingress-editor.rule=Host(`ingress-editor.${DOMAIN_NAME}`)
      - traefik.http.routers.ingress-editor.middlewares=admin-oauth@file
      - traefik.http.services.ingress-editor.loadbalancer.server.port=8001
      - traefik.docker.network=traefik_proxy
    volumes:
      - ${DOCKER_DIR}/traefik/rules:/shared
networks:
  traefik_proxy:
    external: true
  default:
    driver: bridge
